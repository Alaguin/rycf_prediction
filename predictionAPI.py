# -*- coding: utf-8 -*-
"""
Created on Sat Aug 28 15:26:24 2021

@author: alagu
"""



import json
import flask
from flask import request, jsonify
import matplotlib.pyplot as plt
import pandas as pd


app = flask.Flask(__name__)
app.config["DEBUG"] = True




import_file_path = 'Simulation_Calcul_RYCF.xlsx'
read_file = pd.read_excel (import_file_path,names=['transport', 'km', 'co2','total'],engine='openpyxl',skiprows=2,nrows=6 ,usecols="A:D")
read_file.to_csv(r'..\prediction.csv',header=True,index_label=0)
df = pd.read_csv(r'..\prediction.csv')


totalVoiture=df['total'].sum()
totalVoitureMensuel=totalVoiture*4

def iter_month(totalVoitureMensuel):
    arrayData=[[],[]]
   
    i=1
    while i < 13 :
        t=totalVoitureMensuel
        
        arrayData[0].append(i)
      
        if i > 5 and i < 9:
            t=t+t*0.06
            arrayData[1].append(t)
        else :
            arrayData[1].append(t)
        i=i+1
       
    return arrayData[1]
    



# A route to return all of the available entries in our catalog.
@app.route('/api/v1/ressources/pred', methods=['GET'])
def api_all():
    
    data=iter_month(totalVoitureMensuel)  
    str1 = ';'.join(str(e) for e in data)
    return str1


app.run()